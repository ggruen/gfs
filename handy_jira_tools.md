Jira API fun
============

You can use these as inspiration for your gfs hooks.

Parse the IDs of attachments whose filename matches a pattern out of the Jira response.

    # usage: extract_diff_attachment_ids < json_string.txt
    # Given a JSON object in STDIN, outputs a space-separated list of attachment
    # IDs found in the string.
    extract_diff_attachment_ids() {
        # Use perl to parse the attachment IDs out of the JSON object Jira provides.
        # We use perl instead of jq because perl's pre-installed on almost everything.
        # -MJSON: Use the JSON module
        # -n: Loop through all incoming lines
        # -0777: Slurp the entire file (see 'man perlrun', serach for 0777)
        # "_diffs_.*\.diff$": Only extract attachments whose filename matches the
        # format we use.
        perl -MJSON -n0777 \
            -e '$j = decode_json($_);' \
            -e 'map {' \
            -e '    if ( $_->{filename} =~ /_diffs_.*\.diff$/smo ) {' \
            -e '        print "$_->{id} "' \
            -e '    }' \
            -e '} @{$j->{fields}->{attachment}};' \
            -
    }

Given a list of attachment IDs, remove them (no Jira issue needed).

    # usage: delete_attachments attachment_id [attachment_id ...]
    # Example: delete_attachments 12345 23456
    # Set "TEST_ATTACHMENT_EXTRACTION" to test w/o deleting.
    delete_attachments() {
        attachment_ids="$@"
        tracker_domain=$(gfs config read tracker_domain)
        for attachment_id in $attachment_ids ; do
            # If the attachment_id isn't all numbers, warn and return.
            if [[ $attachment_id =~ ^[0-9]+$ ]] ; then
                if [ $TEST_ATTACHMENT_EXTRACTION ] ; then
                    echo "I'd delete attachment ID $attachment_id"
                else
                    curl -s --netrc -X DELETE \
                        "https://${tracker_domain}/rest/api/2/attachment/$attachment_id"
                fi
            else
                echo "WARNING: Attempted to remove invalid attachment ID:" \
                    "\"$attachment_id\"" >&2
            fi

        done

        return 0
    }

Example using gfs to delete attachments matching a pattern from a
specified jira issue.

    delete_attachments $(gfs tracker jira read -t "$jira_issue" -f attachment | \
        extract_diff_attachment_ids)

Upload the current diffs for the current branch to the Jira issue.

    echo "Attaching new diffs..." >&2
    {
        develop_branch=$(gfs config read develop_branch)
        tracker_domain=$(gfs config read tracker_domain)
        issue="$jira_issue" && \
        diff_file="/tmp/${jira_issue}_diffs_`date +%Y-%m-%d`.diff" && \
        git diff origin/${develop_branch}...HEAD  > "$diff_file" && \
        curl -s -D- --netrc -X POST -H "X-Atlassian-Token: nocheck" \
            -F "file=@${diff_file}" \
            "https://${tracker_domain}/rest/api/2/issue/${jira_issue}/attachments" && \
        rm "$diff_file"
    } | grep "HTTP/1.1 200" > /dev/null || \
        error "Upload to Jira failed - check your username and password in .netrc" \
            "and make sure you supplied a Jira issue that exists."

Upload a file list to a custom field in Jira.

    echo "Updating file list in $jira_issue..." >&2
    if gfs tracker jira update -t "$jira_issue" -f customfield_12345="$(gfs files | cut -f2)" ; then
        echo "File list updated." >&2
    else
        echo "File list update failed." >&2
    fi
