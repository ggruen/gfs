#!/usr/bin/env bash
# Read the man page for this script by running perldoc on it.

# Stop the script if any command exits with non-zero exit status and
# isn't caught (e.g. by && or ||).
set -e

# Error if any variables are undefined.
set -u

# error "error string"
# Given a string, outputs the string and exits the script with a non-zero
# status.  Used to report errors.  The script will exit before error exits
# with non-zero status, and set -e above will then abort the script.
#
# Example
# do_something || error "something failed!"
#
error() {
    echo "$@" >&2
    exit 1
}

# Username of our test user - used in "username" and "update"
TEST_USERNAME="testuser"

update() {
    OPTIND=1
    assignee=""
    ticket_status=""
    comment=""
    ticket=""
    branch_name=""
    update_branch_name="" # Allows setting -b ""
    while getopts a:s:t:c:b: OPT; do
        case "$OPT" in
            a) assignee="$OPTARG" ;;
            s) ticket_status="$OPTARG" ;;
            c) comment="$OPTARG" ;;
            t) ticket="$OPTARG" ;;
            b) branch_name="$OPTARG" ; update_branch_name=1 ;;
            \?) error "Unexpected argument: $OPT" ;;
        esac
    done

    # Ticket is required, so error like a tracker should
    test "$ticket" || error "Ticket ID required"

    # Simulate an error
    test "$ticket" = "BAD_TICKET" && error "Invalid Ticket: $ticket"

    test "$assignee" = "ME" && assignee="$TEST_USERNAME"

    # For our mock tracker, output what we received into a file named after our
    # ticket
    ticket_file="/tmp/gfs_tracker_mock-$ticket"
    touch "$ticket_file" # just to be sure it exists
    {
        echo "command:$command"
        echo "ticket:$ticket"
        test "$assignee" = "UNASSIGNED" && { echo "assignee:" ; assignee="" ; }
        test "$assignee" && echo "assignee:$assignee"
        if [ "$ticket_status" ] ; then
            status_name=$(get_statuses | grep -E "^${ticket_status}:" | \
                cut -d: -f2)
            test "$status_name" || error "Invalid status: $ticket_status"
            echo "status:$status_name"
        fi
        test "$comment" && echo "comment:$comment"
        if [ "$update_branch_name" ] && [ "$(gfs config read branch_field)" ] ; then
            echo "branch_name:$branch_name"
        fi
    } > "${ticket_file}.new"

    # Merge the "old" and "new" files into the "old" file.
    # -j 1 means join on the first field
    # -t : means use : as the field separator
    # -a 1 means use all lines from the first file, even if there's no match in
    #      the second file
    # -a 2 means use all lines from the second file, even if there's no match
    #      in the first file
    # cut -d: -f1-2 means to only take the first two fields of the output
    #      (which will be the field name and the value from the first file, or
    #      if there's no value in the first file, the value from the second
    #      file). Note that this won't behave if there's a : in the field
    #      value.
    join -j 1 -t : -a 1 -a 2 \
        <(sort -u "${ticket_file}.new") \
        <(sort -u "$ticket_file") | \
        cut -d: -f1-2 \
        > "${ticket_file}.merged" && mv "${ticket_file}.merged" "$ticket_file"
}

read_ticket() {
    OPTIND=1
    assignee=""
    ticket_status=""
    ticket=""
    branch_name=""
    fields=()
    while getopts ast:b OPT; do
        case "$OPT" in
            a) fields+=("assignee") ;;
            s) fields+=("status") ;;
            t) ticket="$OPTARG" ;;
            b) fields+=("branch_name") ;;
            \?) error "Unexpected argument: $OPT" ;;
        esac
    done

    # Ticket is required, so error like a tracker should
    test "$ticket" || error "Ticket ID required"

    # Simulate an error
    test "$ticket" = "BAD_TICKET" && error "Invalid Ticket: $ticket"

    for fieldname in "${fields[@]}" ; do
        grep -E "^${fieldname}:" /tmp/gfs_tracker_mock-"$ticket"
    done

}

get_statuses() {
    echo "1:To Do"
    echo "2:In Development"
    echo "3:Done"
}

# test_connection tracker_domain
#
# Mock connection test. If domain is "bad_connection", test_connection will
# output "connection_failed" into /tmp/gfs_tracker_mock-test_connection and
# return 1. Otherwise, it will output "connection succeeded" to the same file
# and return 0. This allows unit tests to check
# /tmp/gfs_tracker_mock-test_connection to see if the connection test failed or
# succeeded.
test_connection() {
    domain=${1:-}
    if [ "$domain" = "bad_connection" ] ; then
        echo "connection failed" > /tmp/gfs_tracker_mock-test_connection
        return 1
    else
        echo "connection succeeded" > /tmp/gfs_tracker_mock-test_connection
        return 0
    fi
}

command="$1" ; shift

case $command in
    "update") update "$@" ;;
    "read" ) read_ticket "$@" ;;
    "statuses") get_statuses "$@" ;;
    "username") printf "%s" "$TEST_USERNAME" ;;
    "test") test_connection "$@" ;;
esac

exit 0

# Docs. Read by runing perldoc on this file
: <<END_OF_DOCS
=head1 NAME

gfs_tracker_mock - Mock tracker used for testing or stub tracker script

=head1 SYNOPSIS

    gfs_tracker_mock update [-a assignee_username] \
        [-s ticket_status] \
        [-c comment] \
        [-b branch_name] \
        -t ticket_id

    gfs_tracker_mock read [-a] [-s] [-b]

    gfs_tracker_mock statuses

    gfs_tracker_mock username

    gfs_tracker_mock test [bad_connection]

=head1 OPTIONS

=head1 DESCRIPTION

All tracker scripts must support the syntax in the SYNOPSIS.
Unit tests set the test config file to contain "issue_tracker:mock",
which causes this script to be called.

"gfs_tracker_mock update -t BAD_TICKET" will display an error message and exit
1. This can be used to test failures.

=cut
END_OF_DOCS

