#!/usr/bin/env bash

test -e "$HOME/.gfs/TEST" && {
    echo "Error: ~/.gfs/TEST exists. Please rename or delete it or you'll" \
    "get unpredictable results."
    exit 1
}

#echo "gfs_init_test.sh"
#./gfs_init_test.sh
#
#echo "gfs_config_read_test.sh"
#./gfs_config_read_test.sh
#
#echo "gfs_feature_start_test.sh"
#./gfs_feature_start_test.sh
#
#echo "gfs_feature_finish_test.sh"
#./gfs_feature_finish_test.sh
#
#echo "gfs_tracker_jira_test.sh"
#./gfs_tracker_jira_test.sh

tests=$(echo ./*_test.sh | sed 's#./##g')
runner_passing_=0

for t in ${tests}; do
  echo
  echo "--- Executing the \"${t}\" test suite. ---"
  ( "./${t}" 2>&1; )
  test "${runner_passing_}" -eq 0 -a $? -eq 0
  runner_passing_=$? # Sets runner_passing_ to the output of the "test" command
done

test $runner_passing_ -eq 0 && echo "Tests passed" || echo "Tests failed"
exit ${runner_passing_}
