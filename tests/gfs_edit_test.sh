#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p /tmp/gfs_test_repo/.gfs/TESTCONFIG
    echo "
issue_tracker:mock
release_done_status:3
develop_branch:develop
master_branch:master
" > /tmp/gfs_test_repo/.gfs/TESTCONFIG/config

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TESTCONFIG

    # Stop git from listing untracked files in every test
    echo ".gfs" >> .gitignore
    git add .gitignore && git commit -m "Ignore .gfs"

    # Make develop branch from master
    git checkout -b develop master

    # Add some test features
    mkdir -p ~/.gfs/TESTCONFIG
    {
        echo "testfile"
        echo "testfile1"
        echo "file with spaces"
    } > ~/.gfs/TESTCONFIG/feature1.txt

    {
        echo "testfile2"
        echo "testfile3"
        echo "another file with spaces"
    } > ~/.gfs/TESTCONFIG/feature2.txt
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
    gfs feature start -t "TEST-1" -b "test_feature"
    echo "I made some changes" > testfile
    git add testfile
    git commit -m "TEST-1 made changes"
    gfs feature finish -t "TEST-1"

    gfs release start -t "RTEST-1" -b "test_release"
}

tearDown() {
    test -f testfile && { git rm testfile ; git commit -m "Cleanup" ; }
    git checkout master
    git branch -D test_release
    cd "$SCRIPTDIR" || exit 1
}

testCanListFeatures() {
    result=$(gfs edit -s)
    assertContains "gfs edit -s should list feature1" "$result" "feature1"
    assertContains "gfs edit -s should list feature2" "$result" "feature2"
    assertNotContains "Features shouldn't include .txt extension" \
        "$result" ".txt"
}

testCanEditMultipleFiles() {
    # Given a list of filenames
    # When gfs_edit is run
    export EDITOR=gfs_mock_editor
    result=$(gfs edit -f feature1 | grep -E "^arguments:" | cut -d: -f2)
    # Then it passes all the files to the editor
    assertEquals "testfile testfile1 file with spaces" "$result"
    return 0
}

testCanEditFilesWithSpaces() {
    # Given a list of 3 files, which have spaces in the filenames
    # When gfs_edit is run
    export EDITOR=gfs_mock_editor
    result=$(gfs edit -f feature1 | grep -E "^number:" | cut -d: -f2)
    # Then it passes three files to the editor
    assertEquals "3" "$result"
    return 0
}

testIncludesListFileWhenAskedTo() {
    # When gfs_edit is run with the -l flag
    export EDITOR=gfs_mock_editor
    result=$(gfs edit -l -f feature1 | grep -E "^arguments:" | cut -d: -f2)

    # It opens the file that contains the list of files to edit
    assertContains "$result" "feature1.txt"
    return 0

}

oneTimeTearDown() {
    ./test_teardown.sh

    rm -rf ~/.gfs/TESTCONFIG
}

# shellcheck disable=SC1091
. ./shunit2
