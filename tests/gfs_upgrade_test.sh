#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

#oneTimeSetUp() {
#    ./test_setup.sh
#    mkdir -p /tmp/gfs_upgrade_test || exit 1
#    cd /tmp || exit 1
#    git clone https://gitlab.com/ggruen/gfs.git
#    cd gfs || exit 1
#    make install --prefix=/tmp/gfs_upgrade_test
#}
#
#setUp() {
#    cd /tmp/gfs_test_repo || exit 1
#}
#
tearDown() {
#    rm -rf /tmp/gfs /tmp/gfs_upgrade_test
#    cd /tmp/gfs_test_repo || exit 1
#    test -e .gfs && rm -r .gfs
    cd "$SCRIPTDIR" || exit 1
}

testSetsPrefixToOneLevelAboveBinDir() {
    # Given gfs_upgrade is in $SCRIPTDIR/../gfs_upgrade
    cd "$SCRIPTDIR"/../.. || return 1
    install_dir=$( pwd -P )

    # When gfs_upgrade is run
    output=$(gfs upgrade -n)

    # Then it sets the make prefix to the absolute path to $SCRIPTDIR/../..
    assertContains "gfs upgrade -n output: $output" "$output" \
        "Installing in $install_dir..."

    return 0
}

testDoesNotInstallIfDryRunSpecified() {
    # Given gfs_upgrade is in $SCRIPTDIR/../gfs_upgrade
    cd "$SCRIPTDIR"/.. || return 1
    install_dir=$( pwd -P )

    # When gfs_upgrade is run with the -n flag
    output=$(gfs upgrade -n)

    # Then it doesn't install gfs
    test -e bin ; rc=$?
    assertFalse "$install_dir/bin should not exist" $rc
    test -e man ; rc=$?
    assertFalse "$install_dir/man should not exist" $rc

    return 0
}

testUninstallsIfUninstallFlagSet() {
    # Given gfs in installed in /tmp/gfs_test_install/bin/gfs
    cd "$SCRIPTDIR"/.. || return 1
    make prefix=/tmp/gfs_test_install install
    test -e /tmp/gfs_test_install/bin/gfs || {
        echo "Internal error: 'make prefix=/tmp/gfs_test_install install'" \
            "failed to install gfs" >&2
        exit 1
    }

    # When gfs upgrade -u is run
    /tmp/gfs_test_install/bin/gfs_upgrade -u

    # Then it uninstalls the gfs scripts and man pages in /tmp/bin and /tmp/man
    # We just spot-check gfs. Could be more thorough though.
    test -e /tmp/gfs_test_install/bin/gfs
    assertFalse "gfs upgrade -u should uninstall gfs" $?
    test -e /tmp/gfs_test_install/man/man1/gfs.1
    assertFalse "gfs upgrade -u should uninstall gfs man pages" $?

    rm -rf /tmp/gfs_test_install
    return 0
}

testCleansUpTemporaryDir() {
    # Given /tmp/gfs doesn't exist before "gfs upgrade" runs
    rm -rf /tmp/gfs

    # When gfs upgrade finishes running
    gfs upgrade -n  # Still clones into /tmp/gfs with -n, just doesn't "make"

    # Then /tmp/gfs doesn't exist
    test -e /tmp/gfs
    assertFalse "gfs upgrade should remove /tmp/gfs" $?

    return 0
}

oneTimeTearDown() {
    rm -rf /tmp/gfs /tmp/gfs_upgrade_test
#    ./test_teardown.sh
#
#    # Clean up .netrc. We create the file and set the permissions first to make
#    # sure there's no point at which the data in ~/.netrc is readable by group
#    # or other.  To be paranoid, after creating the new file, we set the
#    # permissions again (just in case the OS decides to use umask and reset the
#    # permissions when clobbering the file in ">".
#    touch ~/.netrc.gfs_cleanup && chmod 600 ~/.netrc.gfs_cleanup
#    # This deletes lines from the line that matches the first pattern to the
#    # line that matches the second pattern inclusively.
#    sed '/# Replace USERNAME and API_PASSWORD/,/machine test\.domain\.qqq/d' \
#        ~/.netrc > ~/.netrc.gfs_cleanup && \
#        chmod 600 ~/.netrc.gfs_cleanup && \
#        mv ~/.netrc.gfs_cleanup ~/.netrc
#    if grep "machine test.domain.qqq" ~/.netrc > /dev/null ; then
#        echo "ERROR: cleanup of ~/.netrc failed. Check the code that adds " \
#            "the entry to .netrc and make sure it matches what's in this " \
#            "unit test." >&2
#         exit 1
#     fi
}

# shellcheck disable=SC1091
. ./shunit2
