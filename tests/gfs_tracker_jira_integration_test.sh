#!/usr/bin/env bash

# Don't blow up automated tests - just exit if called w/no arguments
[ $# -eq 0 ] && exit 0

USAGE="usage: $(basename "$0") -d myjira.atlassian.net -i issue_code -u test_username"

while getopts hvd:i: OPT; do
    case "$OPT" in
        h)  # help
            perldoc "$0"
            exit 0
            ;;
        v) # script version
            echo "$(basename "$0") version 0.1"
            exit 0
            ;;
        d) # Jira domain
            DOMAIN="$OPTARG"
            ;;
        i) # Jira test issue
            ISSUE="$OPTARG"
            ;;
        \?) # Unexpected argument - usage error
            # getopts issues an error message
            echo "$USAGE" >&2
            exit 1
            ;;
    esac
done

# Remove the switches we parsed above.
shift $((OPTIND - 1))

# Get the user's USERNAME
USERNAME=$(awk "/$DOMAIN/"'{print $4}' ~/.netrc)
test "$USERNAME" || {
    echo "Please set up your ~/.netrc file to contain a line like this for" \
        "your Jira instance:" >&2
    echo "machine myinstance.atlassian.net username USERNAME password" \
        "PASSWORD" >&2
    exit 1
}

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:jira
tracker_domain:$DOMAIN
" > /tmp/gfs_test_repo/.gfs/TEST/config

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TEST
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
}

tearDown() {
    cd "$SCRIPTDIR" || exit 1
}

# If Jira returns "HTTP/1.1 204", the exit code should be 0
testCanAddComment() {
    gfs tracker jira update -t "$ISSUE" -c "This is a test comment"
    result=$?
    assertTrue "Adding a comment shouldn't fail" $result

}

# If Jira returns "HTTP/1.1 500", the exit code should be not 0
testCanUpdateDescriptionField() {
    # Update the field
    gfs tracker jira update -t "$ISSUE" -f "description=This is a test description"
    result=$?
    assertTrue "Updating the Description field shouldn't fail" $result

    # Read it back
    output=$(gfs tracker jira read -t "$ISSUE" -f "description")
    # shellcheck disable=SC2016
    assertTrue "Endpoint should be issue/TEST-123 for field update" \
        '[[ $output =~ "This is a test description" ]]'
}

testCanUnassignIssue() {
    # Update the assignee
    gfs tracker jira update -t "$ISSUE" -a "$USERNAME"
    result=$?
    assertTrue "Assigning the issue to $USERNAME shouldn't fail" $result

    # Read it back
    output=$(gfs tracker jira read -t "$ISSUE" -f "assignee")
    # shellcheck disable=SC2016
    assertTrue "Assignee should be $USERNAME" '[[ $output =~ $USERNAME ]]'

    # Unassign the issue
    gfs tracker jira update -t "$ISSUE" -a "UNASSIGNED"
    result=$?
    assertTrue "Unassigning the issue shouldn't fail" $result

    # Read the assignee - make sure it's unassigned ({"assignee":null})
    # shellcheck disable=SC2034
    assignee=$(gfs tracker jira read -t "$ISSUE" -a)
    # shellcheck disable=SC2016
    assertEquals "Assignee should be null" "assignee:" "$assignee"
}

testReturnsStatus() {
    # Make sure it returns a status code, not a JSON string. We just check for
    # one or more letters, and no commas
    gfs tracker jira read -t "$ISSUE" -s | grep -E 'status:[a-zA-Z]+' | \
        grep -v ',' > /dev/null
    result=$?
    assertTrue $result
}

testReturnsAssignee() {
    # Set the username to our provided username
    gfs tracker jira update -t "$ISSUE" -a "$USERNAME"
    # Ask gfs_tracker_jira for the assignee and make sure it's the one we
    # provided.
    gfs tracker jira read -t "$ISSUE" -a | \
        grep -E '^assignee:'"$USERNAME"'$' > /dev/null
    result=$?
    assertTrue $result
}

testCanTransitionStatus() {
    # Get the first available transition
    first_transition=$(gfs tracker jira transitions -t "$ISSUE" | \
        head -1)
    assertNotNull "gfs tracker jira transitions -t $ISSUE should return a transition" "$first_transition"
    transition_id=$(echo "$first_transition" | cut -d: -f1)
    transition_name=$(echo "$first_transition" | cut -d: -f2)

    # Transition to it
    gfs tracker jira update -t "$ISSUE" -s "$transition_id"

    # Read the status
    status=$(gfs tracker jira read -t "$ISSUE" -s | cut -d: -f2)

    # Make sure we got the right status. Note that this isn't always reliable,
    # as the transition name might not be the same as the status.
    assertEquals "$transition_name" "$status"
}

testCanAssignToME() {
    # Unassign the issue
    gfs tracker jira update -t "$ISSUE" -a "UNASSIGNED"
    result=$?
    assertTrue "Unassigning the issue shouldn't fail" $result

    # Read the assignee - make sure it's unassigned ({"assignee":null})
    # shellcheck disable=SC2034
    output=$(gfs tracker jira read -t "$ISSUE" -a)
    # shellcheck disable=SC2016
    assertEquals "assignee:" "$output"

    # Now assign it to ME
    gfs tracker jira update -t "$ISSUE" -a "ME"
    output=$(gfs tracker jira read -t "$ISSUE" -a)
    assertNotContains "$output" 'assignee:ME'
    assertNotContains "$output" 'assignee:null'
    assertContains "$output" "assignee:$USERNAME"
}

testPassesBasicConnectionTest() {
    result=0 ; gfs tracker jira test || result=$?

    assertTrue "gfs tracker jira test should pass" $result
}

oneTimeTearDown() {
    ./test_teardown.sh
}

# shellcheck disable=SC1091
. ./shunit2

# Docs. Read by runing perldoc on this file
: <<END_OF_DOCS
=head1 NAME

gfs_tracker_jira_integration_test.sh - Test gfs_tracker_jira against Jira

=head1 SYNOPSIS

    gfs_tracker_jira_integration_test.sh \
        -d myjira.atlassian.net \
        -i issue_code

=head1 OPTIONS

=head1 DESCRIPTION

Runs update tests on the specified issue in the specified jira domain using
gfs_tracker_jira. You should create a test issue in your Jira instance, then
run this script specifying the instance's domain and the test jira issue.
gfs_tracker_jira_integration_test.sh will then try to access and update a
test field, update the assignee to the default assignee (usually unassigned)
and back to your username (as defined in ~/.netrc), and add a comment.
It will also try to update the issue's status and assign it to "ME"
(which should assign it to your username as defined in ~/.netrc).

=cut
END_OF_DOCS

