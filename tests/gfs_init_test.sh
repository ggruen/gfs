#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
}

tearDown() {
    cd /tmp/gfs_test_repo || exit 1
    test -e .gfs && rm -r .gfs
    cd "$SCRIPTDIR" || exit 1
}

testRequiresTrackerDomain() {
    # We're calling gfs_init without passing "-t tracker_domain"
    if gfs init -q -d "/tmp/gfs_test_repo" TEST 2>/dev/null ; then
        fail "gfs_init should fail if -t tracker_domain isn't provided"
    else
        assertTrue "${SHUNIT_TRUE}"
    fi
}

testTestsConnectionToIssueTracker() {
    # Given a mock issue tracker and tracker domain
    issue_tracker="mock"
    tracker_domain="test.domain.qqq"

    # When gfs_init is called
    test -f /tmp/gfs_tracker_mock-test_connection && \
        rm /tmp/gfs_tracker_mock-test_connection

    rc=0 ; gfs init -q -d "/tmp/gfs_test_repo" -t "$tracker_domain" \
        -i "$issue_tracker" TEST 2>/dev/null || rc=$?
    assertTrue "gfs init should exit with 0 exit code" $rc

    # Then it calls gfs tracker test wtih the domain name
    result=$(cat /tmp/gfs_tracker_mock-test_connection)
    assertEquals "connection succeeded" "$result"

    test -f /tmp/gfs_tracker_mock-test_connection && \
        rm /tmp/gfs_tracker_mock-test_connection
}

testCreatesNetrcForCustomDomain() {
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST
    machine=$(grep "test.domain.qqq" ~/.netrc)
    assertEquals "gfs_init didn't add test.domain.qqq to ~/.netrc" \
        "machine test.domain.qqq login USERNAME password API_PASSWORD" "$machine"
}

testDefaultsFeatureBranchPrefixToFeature() {
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST
    feature_branch_prefix=$(gfs config read -p TEST feature_branch_prefix)

    assertEquals "feature/" "$feature_branch_prefix"
}

testDefaultsReleaseBranchPrefixToRelease() {
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST
    release_branch_prefix=$(gfs config read -p TEST release_branch_prefix)

    assertEquals "release/" "$release_branch_prefix"
}

testUsesGitFlowFeatureBranchPrefixIfExists() {
    # Given that a git flow feature branch prefix has been configured for the
    # repository
    git config gitflow.prefix.feature "test_feature/"
    assertEquals "test_feature/" "$(git config --get gitflow.prefix.feature)"

    # When gfs init is run
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST

    # Then the feature_branch_prefix param is set to the prefix configured by
    # git flow
    feature_branch_prefix=$(gfs config read -p TEST feature_branch_prefix)
    assertEquals "test_feature/" "$feature_branch_prefix"

    # Clean up
    git config --unset gitflow.prefix.feature
}

testUsesGitFlowReleaseBranchPrefixIfExists() {
    # Given that a git flow release branch prefix has been configured for the
    # repository
    git config gitflow.prefix.release "test_release/"
    assertEquals "test_release/" "$(git config --get gitflow.prefix.release)"

    # When gfs init is run
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST

    # Then the release_branch_prefix param is set to the prefix configured by
    # git flow
    release_branch_prefix=$(gfs config read -p TEST release_branch_prefix)
    assertEquals "test_release/" "$release_branch_prefix"

    # Clean up
    git config --unset gitflow.prefix.release
}

testDefaultsDevelopBranchToDevelop() {
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST

    develop_branch=$(gfs config read -p TEST develop_branch)
    assertEquals "develop" "$develop_branch"
}

testDefaultsMasterBranchToMaster() {
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST
    master_branch=$(gfs config read -p TEST master_branch)
    assertEquals "master" "$master_branch"
}

testusesGitFlowDevelopBranchIfExists() {
    # Given that a git flow develop branch has been configured for the
    # repository
    git config gitflow.branch.develop "test_develop"
    assertEquals "test_develop" "$(git config --get gitflow.branch.develop)"

    # When gfs init is run
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST

    # Then the develop_branch param is set to the develop branch configured by
    # git flow
    develop_branch=$(gfs config read -p TEST develop_branch)
    assertEquals "test_develop" "$develop_branch"

    # Clean up
    git config --unset gitflow.branch.develop
}

testusesGitFlowMasterBranchIfExists() {
    # Given that a git flow master branch has been configured for the
    # repository
    git config gitflow.branch.master "test_master"
    assertEquals "test_master" "$(git config --get gitflow.branch.master)"

    # When gfs init is run
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST

    # Then the master_branch param is set to the master branch configured by
    # git flow
    master_branch=$(gfs config read -p TEST master_branch)
    assertEquals "test_master" "$master_branch"

    # Clean up
    git config --unset gitflow.branch.master
}

testDoesntDieIfEDITORNotSet() {
    saved_editor="${EDITOR:-}"
    export EDITOR=
    gfs init -q -d "/tmp/gfs_test_repo" -i "mock" -t "test.domain.qqq" TEST
    result=$?
    assertTrue "gfs init should succeed even if EDITOR is unset" $result
    # Only set it if it was set before. We're assuming that "" means it wasn't
    # set at all.
    test "$saved_editor" && export EDITOR="$saved_editor"
    return 0 # Or the test fails if "saved_editor" is empty
}

testWritesHookScripts() {
    REPO_DIR="/tmp/gfs_test_repo"
    TEST_PROJ="TEST"

    # Given a newly initialized gfs-based repo
    gfs init -q -d "$REPO_DIR" -i "mock" -t "test.domain.qqq" "$TEST_PROJ"

    # When I look in .gfs/hooks
    HOOKS_DIR="$REPO_DIR/.gfs/$TEST_PROJ/hooks"

    # The expected stub scripts exist
    for hook_file in feature_start feature_finish gfs_update_on_develop_branch \
        prepare release_start release_finish_master release_finish_develop ; do
        assertTrue "$hook_file should be written" \
            "[ -f \"$HOOKS_DIR/$hook_file\" ]"

        # And run without error
        if "$HOOKS_DIR/$hook_file" ; then
            assertTrue "$SHUNIT_TRUE"
        else
            fail "$hook_file should run without error"
        fi
    done
}

oneTimeTearDown() {
    ./test_teardown.sh

    # Clean up .netrc. We create the file and set the permissions first to make
    # sure there's no point at which the data in ~/.netrc is readable by group
    # or other.  To be paranoid, after creating the new file, we set the
    # permissions again (just in case the OS decides to use umask and reset the
    # permissions when clobbering the file in ">".
    touch ~/.netrc.gfs_cleanup && chmod 600 ~/.netrc.gfs_cleanup
    # This deletes lines from the line that matches the first pattern to the
    # line that matches the second pattern inclusively.
    sed '/# Replace USERNAME and API_PASSWORD/,/machine test\.domain\.qqq/d' \
        ~/.netrc > ~/.netrc.gfs_cleanup && \
        chmod 600 ~/.netrc.gfs_cleanup && \
        mv ~/.netrc.gfs_cleanup ~/.netrc
    if grep "machine test.domain.qqq" ~/.netrc > /dev/null ; then
        echo "ERROR: cleanup of ~/.netrc failed. Check the code that adds " \
            "the entry to .netrc and make sure it matches what's in this " \
            "unit test." >&2
         exit 1
     fi
}

# shellcheck disable=SC1091
. ./shunit2
