#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

oneTimeSetUp() {
    ./test_setup.sh
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
in_development_status:2
develop_branch:master
branch_field:tracker_branch_field
" > /tmp/gfs_test_repo/.gfs/TEST/config

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TEST
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
}

tearDown() {
    cd "$SCRIPTDIR" || exit 1
}

testErrorsIfTicketNotProvided() {
    gfs commit -m "I forgot to include a ticket"
    assertFalse "gfs commit should error if -t not provided" $?

    return 0
}

testPrependsTicketToCommitMessage() {
    output=$(gfs commit USE_MOCK_GIT_FOR_TESTING -t "ABC-123" -m "Make changes")
    assertContains "$output" "git mock -m:ABC-123 Make changes"

    return 0
}

testPassesArgumentsToGit() {
    output=$(gfs commit USE_MOCK_GIT_FOR_TESTING -t "ABC-123" --argue \
        --blah "argument  with  double  spaces")
    assertContains "$output" "git mock -m:ABC-123"
    assertContains "$output" "git mock --argue"
    assertContains "$output" "git mock --blah"
    assertContains "$output" "argument  with  double  spaces"

    return 0
}

testAssignsTicketToUser() {
    gfs commit USE_MOCK_GIT_FOR_TESTING -t "ABC-123" -m "Make changes"

    mock_output=$(cat /tmp/gfs_tracker_mock-ABC-123)
    mock_username=$(gfs tracker "$(gfs config read issue_tracker)" username)

    # Note: these come from the config set in oneTimeSetUp
    assertContains "$mock_output" "command:update"
    assertContains "$mock_output" "assignee:$mock_username"
    assertContains "$mock_output" "status:In Development"

    return 0
}

testCommitsCodeWhenNoExtraArguementsArePassed() {
    # Given a call to gfs with just the -t and -m arguments
    echo "file for testCommitsCodeWhenNoExtraArguementsArePassed" > testfile
    git add testfile

    # When gfs is run
    gfs commit -t "TEST-123" \
        -m "Test commit for testCommitsCodeWhenNoExtraArguementsArePassed"
    result=$?

    # Then it commits without error
    # (This is due to bug https://gitlab.com/ggruen/gfs/issues/8 - git would
    # fail if no arguemnts were passed beause it would be given "")
    assertTrue "gfs commit should return true when no extra args passed" $result

    return 0
}

testDoesNotPromptIfMessagePassed() {
    # Given a call to gfs with just -t and -m arguments
    echo "file for testDoesNotPromptIfMessagePassed" > testfile
    git add testfile

    # When gfs is run
    editor_storage="$EDITOR"
    export EDITOR="echo"
    output=$(gfs commit -t "TEST-123" \
        -m "Test commit for testDoesNotPromptIfMessagePassed")
    result=$?

    # Then it commits without invoking the editor
    assertNotContains "Editor shouldn't be invoked." \
        "$output" "gfs_test_repo/.git/COMMIT_EDITMSG"

    export EDITOR="$editor_storage"
    return 0
}

testDoesInvokeEditorIfNoMessageProvided() {
    # Given a call to gfs with just -t and -m arguments
    echo "file for testDoesInvokeEditorIfNoMessageProvided" > testfile
    git add testfile

    # When gfs is run
    editor_storage="$EDITOR"
    export EDITOR="echo"
    output=$(gfs commit -t "TEST-123")
    result=$?

    # Then it invokes the editor for a commit message
    assertContains "Editor should be invoked." \
        "$output" "gfs_test_repo/.git/COMMIT_EDITMSG"

    export EDITOR="$editor_storage"
    return 0
}

oneTimeTearDown() {
    ./test_teardown.sh
}

# shellcheck disable=SC1091
. ./shunit2
