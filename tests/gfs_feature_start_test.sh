#!/usr/bin/env bash

# Make sure we're running the scripts we should be testing
SCRIPTDIR=$( pwd -P )
PATH="${SCRIPTDIR}/../:/bin:/usr/bin:/usr/local/bin"

setupPrefixlessConfig() {
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
in_development_status:2
develop_branch:master
branch_field:tracker_branch_field
" > /tmp/gfs_test_repo/.gfs/TEST/config
}

setupConfigWithPrefix() {
    mkdir -p /tmp/gfs_test_repo/.gfs/TEST
    echo "
issue_tracker:mock
in_development_status:2
develop_branch:master
branch_field:tracker_branch_field
feature_branch_prefix:feature/
" > /tmp/gfs_test_repo/.gfs/TEST/config
}

oneTimeSetUp() {
    ./test_setup.sh

    setupPrefixlessConfig

    # Set the project code for the test repo
    cd /tmp/gfs_test_repo || exit 1
    gfs_project_code TEST
    cd "$SCRIPTDIR" || exit 1
}

setUp() {
    cd /tmp/gfs_test_repo || exit 1
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    test "$current_branch_name" = "master" || git checkout master
}

tearDown() {
    test -f testfile && { git rm testfile ; git commit -m "Cleanup" ; }

    # Get us back to master if we're not already there
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    test "$current_branch_name" = "master" || git checkout master

    # Delete the "test_feature" branch if it exists
    git rev-parse --verify test_feature >/dev/null 2>&1 && \
        git branch -D test_feature
    cd "$SCRIPTDIR" || exit 1
}

testMakesNewBranch() {
    gfs feature start -t "TEST-1" -b "test_feature"
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    assertEquals "test_feature" "$current_branch_name"
    git checkout master
    result=$?
    message="Should be able to check out master branch after starting feature"
    assertTrue "$message" $result
    git branch -D test_feature
}

testUpdatesTrackerStatus() {
    gfs feature start -t "TEST-1" -b "test_feature"
    output=$(grep -E '^status:' /tmp/gfs_tracker_mock-TEST-1)
    assertEquals "status:In Development" "$output"
}

testUpdatesBranchInTracker() {
    gfs feature start -t "TEST-1" -b "test_feature"
    output=$(grep -E '^branch_name' /tmp/gfs_tracker_mock-TEST-1)
    assertEquals "branch_name:test_feature" "$output"
}

testAbortsIfHookFails() {
    # Add a hook that'll fail
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/feature_start
    chmod 755 .gfs/TEST/hooks/feature_start

    # Start the feature
    if gfs_feature_start -t "TEST-1" -b "test_feature" ; then
        fail "gfs_feature_start should fail if its hook fails"
    else
        assertTrue 0
    fi

    # Clean up
    rm .gfs/TEST/hooks/feature_start
}

testChecksOutDevelopBranchIfHookFails() {
    # Add a hook that'll fail
    mkdir -p .gfs/TEST/hooks
    echo "#!/bin/sh
    exit 1" > .gfs/TEST/hooks/feature_start
    chmod 755 .gfs/TEST/hooks/feature_start

    # Start the feature
    gfs_feature_start -t "TEST-1" -b "test_feature"

    # Make sure we're on the "develop" branch (which we've configured to be
    # "master" in oneTimeSetup)
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)
    assertEquals "master" "$current_branch_name"

    # Clean up
    rm .gfs/TEST/hooks/feature_start
}

testCreatesBranchWithPrefix() {
    # Add the config file that has the feature_branch_prefix param set to
    # "feature/"
    setupConfigWithPrefix
    gfs_feature_start -t "TEST-1" -b "prefix_feature_test"
    current_branch_name=$(git rev-parse --abbrev-ref HEAD)

    assertEquals "feature/prefix_feature_test" "$current_branch_name"

    # Clean up
    git checkout master
    git branch -D "feature/prefix_feature_test"
    setupPrefixlessConfig
}

testPullsDevelopIfThereIsARemote() {
    git init --bare /tmp/gfs_test_repo_remote
    git remote add origin /tmp/gfs_test_repo_remote
    git push --set-upstream origin master
    output=$(gfs feature start -t "TEST-1" -b "test_feature" 2>&1)
    if echo "$output" | grep -E "Already.up.to.date" > /dev/null ; then
        assertTrue "${SHUNIT_TRUE}"
    else
        echo "output from feature start: $output"
        message="gfs feature start should try to pull remote develop branch "
        message+="if a remote repo exists"
        fail "$message"
    fi
    rm -rf /tmp/gfs_test_repo_remote
    git remote remove origin
}

testDoesntPullDevelopIfThereIsNoRemote() {
    output=$(gfs feature start -t "TEST-1" -b "test_feature" 2>&1)
    if echo "$output" | grep -E "Already.up.to.date" > /dev/null ; then
        message="gfs feature start should try to pull remote develop branch "
        message+="if a remote repo exists"
        fail "$message"
    else
        assertTrue "${SHUNIT_TRUE}"
    fi
}

testPushesNewBranchToRemote() {
    # Given a repo with a remote
    git init --bare /tmp/gfs_test_repo_remote
    git remote add origin /tmp/gfs_test_repo_remote
    git push --set-upstream origin master

    # When gfs_feature_start creates a new branch
    feature_start_output=$(gfs feature start -t "TEST-1" -b "test_feature" 2>&1)

    # Then the branch is pushed to the remote
    git_remote_output=$(git remote show origin)
    if echo "$git_remote_output" | \
        grep "test_feature merges with remote " > /dev/null ; then
        assertTrue "${SHUNIT_TRUE}"
    else
        echo "output from feature start: $feature_start_output"
        message="gfs feature start should push to remote if a remote repo "
        message+="exists."
        fail "$message"
    fi
    rm -rf /tmp/gfs_test_repo_remote
    git remote remove origin
}

oneTimeTearDown() {
    ./test_teardown.sh
}

# shellcheck disable=SC1091
. ./shunit2
